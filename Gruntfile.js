module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),
      terser: {
        options: {
          sourceMap: true
        },
        build: {
          files: {
            'build/js/<%= pkg.name %>.min.js': [
              'node_modules/react/umd/react.development.js',
              'node_modules/react-dom/umd/react-dom.development.js',
              'src/js/<%= pkg.name %>.js'
            ]
          }
        }
      },
      sass: {
        dist: {
          options: {
            style: 'expanded'
          },
          files: {
            'build/css/procrastinationdashtimer.css': 'src/sass/procrastinationdashtimer.scss'
          }
        }
      },
      copy: {
        html: {
            files: [
                {expand: true, src: ['src/html/*'], dest: 'build/', flatten: true},
            ]
        },
        normalize: {
          files: [
            {expand: true, src: ['node_modules/normalize.css/normalize.css'], dest: 'build/css', flatten: true},
          ]
        }
      },
      watch: {
        sass: {
          files: ['src/sass/**'],
          tasks: ['sass'],
          options: {},
        },
        html: {
          files: ['src/html/**'],
          tasks: ['copy:html'],
          options: {},
        }
      },
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-terser');
  
    // Default task(s).
    grunt.registerTask('default', ['terser', 'sass', 'copy', 'watch']);
  
  };