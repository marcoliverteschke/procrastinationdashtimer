# Procrastination Dash Timer

A client-side relaunch of procrastinationdashtimer.com. Bye, bye, CakePHP.

## About

## ToDo

- [X] General project setup
- [ ] Create first mockup of the new UI
- [ ] Start / stop timer with fixed work / break values
- [ ] Write a log of work / break phases onto the page
- [ ] Store current state of the timer in localstorage
- [ ] Retrieve state of timer from localstorage on load
- [ ] Store log of work / break phases for current timer run in local storage
- [ ] Retrieve state of timer from localstorage on load
- [ ] Build tabbed UI for settings form, about section, privacy section
- [ ] Settings form stores custom work / break times in localstorage
- [ ] Work / break times for new timer are retrieved from local storage
- [ ] Implemented theme switching
